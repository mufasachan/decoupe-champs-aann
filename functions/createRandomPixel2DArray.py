import random
import numpy as np

def createRandomPixel2DArray(rayon, imgShape):
    '''
        Fonction qui renvoie un arraylike de dimension (2, nTrain) comportant des pixels tirés aux hasard dans l'image.
        Ces pixels sont tirés de façon à être assez loin de bords de l'image, à une distance égale à rayon. 
        nTrain est déterminé en prenant une marge par rapport au nombre de carrée que nous pouvons prendre dans l'image.
        On prends un facteur 1 / PORTION_CARRE dans l'image des carrés possibles
        @param rayon : int, rayon est la distance entre les pixels selectionnés et le bord de l'image
        @param imgShape : arraylike (N:int,N:int,nBande:int), la dimension de l'image
        @return nRandomPixel : arraylike (2, nTrain:int), le fameux tableau avec plein de pixel à l'intérieur
        @return nTrain : int, le nombre  de pixel que nous dans le tableau que nous avons.
    '''
    PORTION_CARRE = 4
    perimetre = 2*rayon + 1 # Périmètre 
    nPixel = (imgShape[0] - 2*rayon) * (imgShape[1] - 2*rayon) # Nombre de pixel dans l'image possible
    nCarre = int(nPixel / perimetre**2) # Nombre de carrée de l'image
    nTrain =  int(nCarre / PORTION_CARRE)# Nombre de carrée d'entrainement
    #   On tire au hasard nTrain numéro de pixel sur les nPixel disponible, de façon unique (i.e on tire pas deux fois le même pixel)
    nRandomCoordinate = random.sample(range(nPixel), nTrain)
    #   On créer le tableau de toutes les coordonnées possibles pour faire notre set d'entrainement
    coordinateTab = np.zeros([nPixel,2]) # Tableau regroupant toutes les coordonnées possibles
    print(nPixel)
    for i in range(imgShape[0]- 2*rayon):
        for j in range(imgShape[1]- 2*rayon):
            coordinateTab[i*(imgShape[1]-2*rayon) + j] = [i+rayon,j+rayon]
    coordinateTab = np.reshape(coordinateTab, [-1,2])

    nRandomPixel = np.zeros([2, nTrain])
    for i in range(nTrain):
        nRandomPixel[:,i] = coordinateTab[nRandomCoordinate[i],:]
    nRandomPixel = nRandomPixel.astype(np.uint16)

    return nRandomPixel, nTrain
