import numpy as np

def img_seuil(img, seuil_range):
    """
    Seuil l'image
    :param img: L'image 2-D à seuiller (Nx,Ny) float / int
    :param seuil_range: La fouchette autorisée de valeur pour le seuil
    :return img_s: L'image seuillée
    """
    #   Variable de travail
    img_s = np.zeros(img.shape)

    #   Seuillage
    for i in range(len(img_s)):
        for j in range(len(img_s[i])):
            if img[i,j] >= seuil_range[0] and img[i,j] <= seuil_range[1]:
                img_s[i,j] = img[i,j]
            else:
                img_s[i,j] = 0

    #   OP Done
    return img_s
