from skimage import filters
from skimage.morphology import disk

import numpy as np

def calcul_entropie(img):
    """
    Cette fonction prends une image est calcule son entropie locale.
    :param img: L'image a calculé l'entropie, np.array de dimension 3, (Nx, Ny, Nbande)
    :return eL: L'entropie locale calculée
    """
    # Variable de travail
    Nbande = img.shape[2] # Nombre de bande dans l'image
    img_shape = img.shape[:2] # Taille de l'image
    eL = np.zeros(img_shape) # Entropie locale

    # Calcul et sommatation des entropies locales spectrales
    img_n = img / np.max(img) # L'entropy se calcule seulement avec des images allant de 0 à 1
    for i in range(Nbande):
        eL += filters.rank.entropy(img_n[:,:,i], disk(1))
    eL /= Nbande # Moyenne sur l'ensemble des bandes

    return eL # Done
