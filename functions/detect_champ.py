import numpy as np

def detect_champ(map):
    """
    Cette fonction permet de renvoyer une image binaire de la taille de map. Les pixels sont blancs si un champs
    sur la map était présent, les pixels sont noirs si ce n'est pas un champs. Cela retourne donc le masque.
    L'hypothèse que nous avons c'est que les pixels qui ne sont pas des champs sont codés en niveaux de gris, et les
    champs sont en couleurs.
    :param map: La variable image RGB, variable 3-D, deux dimensions d espace et une dimension de couleur. (Nx,Ny,NRVB = 3)
    :return mask: Le masque qui donne la position des champs, variable 2-D. (Nx,Ny)
    """
    #   Variable de travail
    mask = np.zeros(map.shape[:2], dtype=bool)

    #   Construction du masque, les champs ne sont pas des pixels gris
    for i in range(1,len(map)-1):
        for j in range(1,len(map[i])-1):
            m = np.sum(map[i, j, :]) / 3
            if m > map[i, j, 0] - 3 and m < map[i, j, 0] + 3\
            and m > map[i, j, 1] - 3 and m < map[i, j, 1] + 3\
            and m > map[i, j, 2] - 3 and m < map[i, j, 2] + 3:
                mask[i,j] = False
            else: # Gris
                mask[i,j] = True

    #   OP terminé
    return mask
