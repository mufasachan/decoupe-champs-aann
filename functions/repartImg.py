import numpy as np
import matplotlib.pyplot as plt

def repartImg(imgTab, labelTab):
    '''
        Affiche la répartition des couleurs d'une série image en fonction de ces bandes. 
        Les images doivent avoir le même nombre de bande
        @param img : arraylike float(nImage, nX, nY, nBande) : Un tableau d'image à plusieurs bande
        @param labelTab : arraylike String (nImage) : Un tableau de nom de truc à tracer
        @return repartTab : arraylike float (nImage, nBande) : Somme des intensités sur toute l'image par bande
    '''
    if imgTab.ndim == 3:
        print("L'image est monochrome, il n'y a pas de répartition des couleurs possible")
    else:
        nImage = imgTab.shape[0]
        nBande = imgTab.shape[-1]
        repartTab = np.sum(np.sum(imgTab,1),1)
        plt.figure()
        for i in range(nImage):
            plt.plot(range(1, nBande + 1),repartTab[i], label=labelTab[i])
        plt.xticks(range(1,nBande + 1))
        plt.xlabel('Spectral bands')
        plt.ylabel('Sum pixel values') 
        plt.legend()
        plt.grid()
        plt.show()
    return repartTab
