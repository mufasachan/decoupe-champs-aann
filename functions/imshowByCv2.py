import numpy as np
import cv2

def imshowByCv2( nomFenetreTab, imgTab):
    '''
        C'est une fonction pour afficher à partir de imshow le nombre d'image que nous voulons. Appuyer sur ESC (Échap) pour fermer les fenêtres
        @param nomFenetreTab : Tableau de chaine de caractere, le nom des fenêtre que nous voulons afficher
        @param imgTab : Tableau d'image RVB (n,m,3), les images que nous voulons afficher
    '''
    if len(nomFenetreTab) != len(imgTab):
        print("Les tailles des tableaux de noms et d'images ne correspondent pas")
    else:
        n = len(nomFenetreTab)
        for i in range(n):
            if imgTab[i].ndim == 2: # Monocouche
                cv2.namedWindow(nomFenetreTab[i], cv2.WINDOW_NORMAL)
                cv2.imshow(nomFenetreTab[i], 255*imgTab[i])
            else:
                img = (255 * imgTab[i][:,:,0:3]).astype(np.uint8)
                cv2.namedWindow(nomFenetreTab[i], cv2.WINDOW_NORMAL)
                cv2.imshow(nomFenetreTab[i], img)
        finish = False
        while not finish:
            k = cv2.waitKey(0)
            if k == 27:
                cv2.destroyAllWindows()
                finish = True 
